const getClass = (key) => document.querySelector(key); // A function for easy querySelector usage
const getElement = (key) => document.getElementById(key); // A function for easy getElementById usage
// DOM Elements
const grid = getClass(".layerOne");
const currPlayer = getClass(".player");
const winner = getClass(".winner");
// Active player
let active = "X";
// The move making buttons
const moveMakers = [];

// Add rows and columns
for (let i = 0; i < 7; i++) {
	for (let x = 0; x < 7; x++) {
		const html = `
		<div class="box" id="${i}/${x}"></div>
		`;
		grid.insertAdjacentHTML("beforeend", html);

		// If box is outside the 5x5 gameboard make it invisible
		if (new RegExp(/6|0/g).test(`${i}/${x}`)) {
			getElement(`${i}/${x}`).classList.toggle("invisible");
			getElement(`${i}/${x}`).classList.toggle("surrounding");
		}
	}
}
// Set active player to X
currPlayer.textContent = `Urmatoarea mutare: ${active}`;

// Add the event listener
grid.addEventListener("click", clickHandler);

// Player UI
function clickHandler() {
	// Clear the move makers everytime new box is selected
	for (let i of moveMakers) {
		i.classList.toggle("invisible");
	}
	// Clear the array
	moveMakers.splice(0);

	const id = event.target.id;
	let [v, h] = id.split("/"); // The two parts of the ID ("1/3" = 1, 3)
	h = parseInt(h);
	v = parseInt(v);
	const box = getElement(id);

	// If the box is not an edge box in the 5x5 square or box doesn't exist return
	if (!new RegExp(/5|1/g).test(id) || !box) return;
	// Don't do anything if the box is claimed by opponent
	if (box.textContent && box.textContent !== active) return;

	// Save the move makers that the user can use for the selected box
	const ids = []; // Saves the ids of the move makers
	if (new RegExp(/1\/5|5\/1|1\/1|5\/5/g).test(id)) {
		// Logical expression to find which squares to show when corner piece is selected
		ids.push(`${v}/${h < 2 ? "6" : "0"}`, `${v < 2 ? "6" : "0"}/${h}`);
	} else {
		// Logical expression to find which squares to show when edge piece is selected
		if (v === 1 || v === 5)
			ids.push(`${v}/0`, `${v}/6`, `${v < 2 ? "6" : "0"}/${h}`);
		if (h === 1 || h === 5)
			ids.push(`0/${h}`, `6/${h}`, `${v}/${h < 2 ? "6" : "0"}`);
	}
	// Save move maker elements to the array
	for (let x of ids) {
		moveMakers.push(getElement(x));
	}
	// Set the event listners on move makers
	for (let e of moveMakers) {
		// Make buttons visible
		if (e.classList.contains("invisible")) e.classList.toggle("invisible");
		// Handle the click
		e.addEventListener("click", makeMove);
		e.textContent = active;
	}
}

// MoveMakers
function makeMove() {
	let [v, h] = event.target.id.split("/");
	v = parseInt(v);
	h = parseInt(h);
	const data = [];

	// Finds whether the move was a vertical one or horizontal one
	if (v === 6 || v === 0) {
		// For vertical one
		// Loop over the boxes in that column and collect data
		for (let i = 1; i < 6; i++) {
			data.push(getElement(`${i}/${h}`).textContent);
		}
		// Determines if the move was made from top or bottom
		// If move made from top add the new move to the front of array
		// If move made from bottom add the move to the back of array
		if (v < 1) {
			data.unshift(active);
			data.splice(5, 1);
		} else {
			data.push(active);
			data.shift();
		}

		// Showing the data back on the screen
		for (let i = 1; i < 6; i++) {
			getElement(`${i}/${h}`).textContent = data[i - 1];
		}
	} else if (h === 6 || h === 0) {
		// For horizontal one
		const n = h < 1 ? h + 1 : h - 1; // Determines if the move was made from left or right

		// Loop over the boxes in that column and collect data
		for (let i = 1; i < 6; i++) {
			data.push(getElement(`${v}/${i}`).textContent);
		}

		// Determines if the move was made from left or right
		// If move made from left add the new move to the front of array
		// If move made from right add the move to the back of array
		if (h < 1) {
			data.unshift(active);
			data.splice(5, 1);
		} else {
			data.push(active);
			data.shift();
		}

		// Showing the data back on the screen
		for (let i = 1; i < 6; i++) {
			getElement(`${v}/${i}`).textContent = data[i - 1];
		}
	}

	// Stop game if winner found
	const winner = decideWinner();
	if (winner) return;
	active === "X" ? (active = "O") : (active = "X"); // Change active player
	currPlayer.textContent = `Urmatoarea mutare: ${active}`; // Show active player on the browser
	data.splice(0); // Clear the array so its ready for next move
}

// Decide a winner
function decideWinner() {
	let matched = 0; // Default to 0
	const declare = () => {
		grid.removeEventListener("click", clickHandler); // Remove the events
		winner.textContent = `Castigator: ${active}`; // Show winner
		// Clear the move makers
		for (let i of moveMakers) {
			i.classList.toggle("invisible");
			i.removeEventListener("click", makeMove);
		}
		moveMakers.splice(0);
		return true;
	};

	// Diagonal 1
	for (let i = 1; i < 6; i++) {
		const val = getElement(`${i}/${i}`).textContent;
		// Add to matches when got a match
		if (val === active) matched++;
	}
	// Set gameover to true if 5 matches
	if (matched === 5) declare();

	// Diagonal 2
	if (
		getElement("5/1").textContent === active &&
		getElement("4/2").textContent === active &&
		getElement("3/3").textContent === active &&
		getElement("2/4").textContent === active &&
		getElement("1/5").textContent === active
	) {
		declare();
	}

	// Horizonatal checks
	for (let v = 1; v < 6; v++) {
		matched = 0;
		for (let h = 1; h < 6; h++) {
			const val = getElement(`${v}/${h}`).textContent;
			// Add to matches when got a match
			if (val === active) matched++;
		}
		if (matched === 5) {
			declare();
			break;
		}
	}

	// Vertical check
	for (let h = 1; h < 6; h++) {
		matched = 0;
		for (let v = 1; v < 6; v++) {
			const val = getElement(`${v}/${h}`).textContent;
			// Add to matches when got a match
			if (val === active) matched++;
		}
		if (matched === 5) {
			declare();
			break;
		}
	}
}

// p5 elements for design and utilities
function setup(){
    canvas = createCanvas(windowWidth, windowHeight);
    canvas.position(0,0);
    canvas.style('z-index', '-1');
    inputNumeP1();
    inputNumeP2();
}

function draw() {
    rect(1050, 80, 400, 600);
    rules();
    ControlPannel();
    textSize(55); 
    text('QUIXO', 640, 650);
    fill('white');
     
}

function inputNumeP1(){
    numePlayerOne = createInput();
    numePlayerOne.position(1100,195);

    butonPlayerOne = createButton('Alege');
    butonPlayerOne.position(numePlayerOne.x + numePlayerOne.width, 195);
    butonPlayerOne.mousePressed(numeP1);
    textNume1 = createElement('h2', 'Nume jucator X:');
    textNume1.position(1100, 220);
}

function numeP1() {
const numeJucator1= numePlayerOne.value();
textNume1.html('Nume jucator X:' + numeJucator1);
numePlayerOne.value('');
}

function inputNumeP2(){
    numePlayerTwo = createInput();
    numePlayerTwo.position(1100,285);

    butonPlayerTwo = createButton('Alege');
    butonPlayerTwo.position(numePlayerTwo.x + numePlayerTwo.width, 285);
    butonPlayerTwo.mousePressed(numeP2);
    textNume2 = createElement('h2', 'Nume jucator O:');
    textNume2.position(1100, 305);
}

function numeP2() {
const numeJucator2= numePlayerTwo.value();
textNume2.html('Nume jucator O:' + numeJucator2);
numePlayerTwo.value('');
}

function rules(){
 
    rect(80, 70, 350, 450);
    fill('black');
    textSize(25);
    text('Scopul jocului si reguli:', 100, 140);
    textSize(40);
    textStyle(BOLD);
    text('Panou Control',1115,130);
   textSize(14);
    text('- Scopul jocului este sa faci serii de 5 X -uri', 90, 220);
    text('sau 5 O-uri pe orizontala/verticala sau diagonala.', 90, 260);
    text('- Fiecare jucator are o mutare pe runda.', 90, 310);
    text('- Poti misca si pentru a incurca adversaul.', 90, 360);
    text('- E doar un joc, trateaza-l ca atare si nu te enerva.', 90, 410);
    textSize(20);
    text(' Mult noroc, si distractie!', 130, 500);
}

function ControlPannel(){
    fill('white');
    rect(1090, 175, 320, 200);
    rect(1090, 395, 320, 100);
    rect(1090, 530, 320, 60);
    butonReset = createButton('Reseteaza joc');
    butonReset.position(1085, 610);
    butonReset.size(120,50);
    butonReset.mousePressed(resetGame);
    butonReload = createButton('Re-incarca joc');
    butonReload.position(1285, 610);
    butonReload.size(120,50);
    butonReload.mousePressed(reloadGame);
}
// end p5 elements for design and utilities
function resetGame(){
	for (i = 1; i < 6; i++) {
        for (j = 1; j < 6; j++) {
            moveMakers[i + '/' + j] = '';
        }
    }
    // get an array with all playable boxes
    let allBox = document.getElementsByClassName('box');
    // reset all boxes to empty string
    for (let i = 0; i < allBox.length; i++) {
        allBox[i].innerText = '';
    }
}

function reloadGame(){
    window.location.reload(); // simple refresh page line for when the game is over
}